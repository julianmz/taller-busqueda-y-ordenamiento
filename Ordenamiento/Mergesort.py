def mergesort(arr):
    """Ordena una lista utilizando el algoritmo Mergesort."""
    if len(arr) > 1:
        mid = len(arr) // 2  # Encontrar el punto medio de la lista
        left_half = arr[:mid]  # Dividir la lista en la mitad izquierda
        right_half = arr[mid:]  # Dividir la lista en la mitad derecha

        mergesort(left_half)  # Ordenar recursivamente la mitad izquierda
        mergesort(right_half)  # Ordenar recursivamente la mitad derecha

        i = j = k = 0

        # Combinar las dos mitades ordenadas
        while i < len(left_half) and j < len(right_half):
            if left_half[i] < right_half[j]:
                arr[k] = left_half[i]
                i += 1
            else:
                arr[k] = right_half[j]
                j += 1
            k += 1

        # Copiar los elementos restantes de left_half, si hay alguno
        while i < len(left_half):
            arr[k] = left_half[i]
            i += 1
            k += 1

        # Copiar los elementos restantes de right_half, si hay alguno
        while j < len(right_half):
            arr[k] = right_half[j]
            j += 1
            k += 1

# Prueba del algoritmo Mergesort
lista = [3, 6, 8, 10, 1, 2, 1]
print("Lista original:", lista)
mergesort(lista)
print("Lista ordenada con Mergesort:", lista)
