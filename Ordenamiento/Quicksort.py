def quicksort(arr):
    """Ordena una lista utilizando el algoritmo Quicksort."""
    if len(arr) <= 1:
        return arr
    else:
        pivot = arr[len(arr) // 2]  # Elegir el pivote (aquí se elige el elemento del medio)
        left = [x for x in arr if x < pivot]  # Elementos menores que el pivote
        middle = [x for x in arr if x == pivot]  # Elementos iguales al pivote
        right = [x for x in arr if x > pivot]  # Elementos mayores que el pivote
        # Ordenar recursivamente las sublistas y combinarlas con el pivote
        return quicksort(left) + middle + quicksort(right)

# Prueba del algoritmo Quicksort
lista = [3, 6, 8, 10, 1, 2, 1]
print("Lista original:", lista)
print("Lista ordenada con Quicksort:", quicksort(lista))
