def bubble_sort(arr):
    """
    Ordena una lista utilizando el algoritmo Bubble Sort.
    """
    n = len(arr)
    # Recorre todos los elementos de la lista
    for i in range(n):
        # Últimos i elementos ya están en su lugar correcto
        # Se recorren los elementos restantes
        for j in range(0, n-i-1):
            # Si el elemento actual es mayor que el siguiente, intercambiarlos
            if arr[j] > arr[j+1]:
                arr[j], arr[j+1] = arr[j+1], arr[j]

# Prueba del algoritmo Bubble Sort
lista = [64, 34, 25, 12, 22, 11, 90]
print("Lista original:", lista)
bubble_sort(lista)
print("Lista ordenada con Bubble Sort:", lista)
