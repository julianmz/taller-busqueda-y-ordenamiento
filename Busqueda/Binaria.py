def busqueda_binaria(lista, valor):
    izquierda, derecha = 0, len(lista) - 1
    comparaciones = 0

    while izquierda <= derecha:
        comparaciones += 1
        medio = (izquierda + derecha) // 2
        if lista[medio] == valor:
            return medio, comparaciones
        elif lista[medio] < valor:
            izquierda = medio + 1
        else:
            derecha = medio - 1

    return -1, comparaciones

# Ordenar la lista antes de la búsqueda binaria
lista_numeros = [3, 5, 2, 7, 8, 10, 1, 6]
lista_numeros.sort()

# Valores a buscar
valores_buscados = [7, 1, 10]

# Prueba de la búsqueda binaria
for valor in valores_buscados:
    indice, comparaciones = busqueda_binaria(lista_numeros, valor)
    print(f"Valor buscado: {valor}")
    print(f"Índice del valor buscado: {indice}")
    print(f"Número de comparaciones realizadas: {comparaciones}")
    print("-" * 30)
