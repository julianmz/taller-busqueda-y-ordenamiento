def busqueda_lineal(lista, valor):
    comparaciones = 0
    for indice, elemento in enumerate(lista):
        comparaciones += 1
        if elemento == valor:
            return indice, comparaciones
    return -1, comparaciones

# Ejemplo de uso y prueba
lista_numeros = [3, 5, 2, 7, 8, 10, 1, 6]
valor_buscado = 7

indice, comparaciones = busqueda_lineal(lista_numeros, valor_buscado)

print(f"Índice del valor buscado: {indice}")
print(f"Número de comparaciones realizadas: {comparaciones}")
