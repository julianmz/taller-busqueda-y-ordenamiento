import random
import time
import cProfile

# Función para generar una lista de datos aleatorios
def generar_lista_aleatoria(n):
    return [random.randint(1, 10000) for _ in range(n)]

# Implementación de Bubble Sort
def bubble_sort(arr):
    n = len(arr)
    for i in range(n):
        for j in range(0, n-i-1):
            if arr[j] > arr[j+1]:
                arr[j], arr[j+1] = arr[j+1], arr[j]

# Implementación de Quicksort
def quicksort(arr):
    if len(arr) <= 1:
        return arr
    else:
        pivot = arr[len(arr) // 2]
        left = [x for x in arr if x < pivot]
        middle = [x for x in arr if x == pivot]
        right = [x for x in arr if x > pivot]
        return quicksort(left) + middle + quicksort(right)

# Implementación de Mergesort
def mergesort(arr):
    if len(arr) > 1:
        mid = len(arr) // 2
        left_half = arr[:mid]
        right_half = arr[mid:]

        mergesort(left_half)
        mergesort(right_half)

        i = j = k = 0

        while i < len(left_half) and j < len(right_half):
            if left_half[i] < right_half[j]:
                arr[k] = left_half[i]
                i += 1
            else:
                arr[k] = right_half[j]
                j += 1
            k += 1

        while i < len(left_half):
            arr[k] = left_half[i]
            i += 1
            k += 1

        while j < len(right_half):
            arr[k] = right_half[j]
            j += 1
            k += 1

# Generar lista de datos aleatorios para las pruebas
lista_datos = generar_lista_aleatoria(2000)

# Prueba de Bubble Sort
inicio_bubble = time.time()
bubble_sort(lista_datos.copy())
fin_bubble = time.time()
tiempo_bubble = fin_bubble - inicio_bubble
print("Tiempo de Bubble Sort:", tiempo_bubble)

# Prueba de Quicksort
inicio_quick = time.time()
quicksort(lista_datos.copy())
fin_quick = time.time()
tiempo_quick = fin_quick - inicio_quick
print("Tiempo de Quicksort:", tiempo_quick)

# Prueba de Mergesort
inicio_merge = time.time()
mergesort(lista_datos.copy())
fin_merge = time.time()
tiempo_merge = fin_merge - inicio_merge
print("Tiempo de Mergesort:", tiempo_merge)

# Análisis de perfil utilizando cProfile
print("\nAnálisis de Perfil:")
print("Bubble Sort:")
cProfile.run("bubble_sort(lista_datos.copy())")
print("Quicksort:")
cProfile.run("quicksort(lista_datos.copy())")
print("Mergesort:")
cProfile.run("mergesort(lista_datos.copy())")
